# Canopee Child theme

__Author:__ ThemesInFrance  
__Requires at least:__ 5.0  
__Requires PHP:__ 7.0  
__Tested up to:__ 5.4  
__Stable tag:__ 0.1  
__License:__ GPLv2 or later  
__License URI:__ http://www.gnu.org/licenses/gpl-2.0.html

## Description  

This is a sample child theme created for [ThemesInFrance Canopee WordPress theme](https://themesinfrance/).

## Installation  

1. Install Canopee parent theme.
2. In your admin panel, go to Appearance > Themes and click the Add New button.
3. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
4. Click Activate to use your new theme right away.

## Documentation  

> Todo

## Changelog  

### 0.1.0
Initial release  
