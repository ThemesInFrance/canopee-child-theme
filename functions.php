<?php

if ( ! defined('ABSPATH')) exit;  // if direct access

/*
 * Favicons ********************************************************************
 * @link http://realfavicongenerator.net/
 *
**/

// add_action('wp_head','tifchild_custom_favicon', 1);
// function tifchild_custom_favicon() {
// 	$favicon = ''."\n";
// 	$favicon .= '<link rel="apple-touch-icon" sizes="180x180" href="' . get_stylesheet_directory_uri() . '/images/favicon/apple-touch-icon.png">'."\n";
// 	$favicon .= '<link rel="icon" type="image/png" href="' . get_stylesheet_directory_uri() . '/images/favicon/favicon-32x32.png" sizes="32x32">'."\n";
// 	$favicon .= '<link rel="icon" type="image/png" href="' . get_stylesheet_directory_uri() . '/images/favicon/favicon-16x16.png" sizes="16x16">'."\n";
// 	$favicon .= '<link rel="manifest" href="' . get_stylesheet_directory_uri() . '/images/favicon/manifest.json">'."\n";
// 	$favicon .= '<link rel="mask-icon" href="' . get_stylesheet_directory_uri() . '/images/favicon/safari-pinned-tab.svg" color="#5bbad5">'."\n";
// 	$favicon .= '<link rel="shortcut icon" href="' . get_stylesheet_directory_uri() . '/images/favicon/favicon.ico">'."\n";
// 	$favicon .= '<meta name="msapplication-config" content="/images/favicons/browserconfig.xml">'."\n";
// 	$favicon .= '<meta name="theme-color" content="#ffffff">'."\n";
// 	$favicon .= '<!---->'."\n\n";
//
// 	echo $favicon ;
// }
